﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Entities;

namespace Heroes3.Game
{
    class Player
    {
        private List<Unit> units;
        private int money;
        private string name;

        private bool inverseImage;

        public Player(string name, int money, bool inverseImage)
        {
            this.name = name;
            this.money = money;

            units = new List<Unit>();

            this.inverseImage = inverseImage;
        }

        public string Name
        {
            get { return name; }
        }

        public int Money
        {
            get { return money; }
        }

        public void ResetUnitsStep()
        {
            for (int i = 0; i < units.Count; i++)
            {
                units[i].ResetStep();
            }
        }

        public List<Unit> Units
        {
            get { return new List<Unit>(units); }
        }

        public bool ContainsUnit(Unit unit)
        {
            return units.Contains(unit);
        }

        public bool BuyUnit(Unit unit)
        {
            if (money >= unit.Cost)
            {
                money -= unit.Cost;
                units.Add(unit);
                return true;
            }

            return false;
        }

        public void SellUnit(Unit unit)
        {
            money += unit.Cost;
            units.Remove(unit);
        }

        public Unit GetUnitByIJ(int i, int j)
        {
            foreach (Unit unit in units)
            {
                if (unit.I == i && unit.J == j)
                {
                    return unit;
                }
            }

            return null;
        }

        public void DrawUnits(Graphics graphics)
        {
            foreach (Unit unit in units)
            {
                unit.Draw(graphics, inverseImage);
            }
        }

        public void DeleteDeadUnits()
        {
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].Hp <= 0)
                {
                    units.RemoveAt(i);
                    i = 0;
                }
            }
        }

    }
}
