﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Tools;

namespace Heroes3.Game
{
    class BattleField
    {
        private enum TypeCell
        {
            Empty = 0
        }

        private int[,] field;
        private int fieldRows, fieldColumns;

        private Image image;
        private int imageWidth, imageHeight;

        private int iSelected, jSelected;

        public BattleField(int fieldRows, int fieldColumns)
        {
            this.fieldRows = fieldRows;
            this.fieldColumns = fieldColumns;

            field = new int[fieldRows, fieldColumns];

            image = Image.FromFile(IniWorker.GetStringParam("field_image"));
            imageWidth = IniWorker.GetIntParam("field_imageWidth");
            imageHeight = IniWorker.GetIntParam("field_imageHeight");

            ClearField();

            ResetSelectedCell();
        }

        public int Rows
        {
            get { return fieldRows; }
        }

        public int Columns
        {
            get { return fieldColumns; }
        }

        public int Width
        {
            get { return fieldColumns * imageWidth; }
        }

        public int Height
        {
            get { return fieldRows * imageHeight; }
        }

        public int ISelected
        {
            get { return iSelected; }
        }

        public int JSelected
        {
            get { return jSelected; }
        }

        public bool IsCellSelected
        {
            get { return iSelected != -1 && jSelected != -1; }
        }

        private void ClearField()
        {
            for (int i = 0; i < fieldRows; i++)
            {
                for (int j = 0; j < fieldColumns; j++)
                {
                    field[i, j] = (int)TypeCell.Empty;
                }
            }
        }

        public void SetSelectedCell(int x, int y)
        {
            jSelected = x / imageWidth;
            iSelected = y / imageHeight;
        }

        public void ResetSelectedCell()
        {
            iSelected = jSelected = -1;
        }

        public void Draw(Graphics graphics)
        {
            Pen pen = new Pen(Color.Black, 2);
          
            for (int i = 0; i < fieldRows; i++)
            {
                for (int j = 0; j < fieldColumns; j++)
                {
                    graphics.DrawImage(image, j * imageWidth, i * imageHeight, imageWidth, imageHeight);

                    graphics.DrawRectangle(pen, j * imageWidth, i * imageHeight, imageWidth, imageHeight);
                }
            }
        }

        public void DrawSelected(Graphics graphics)
        {
            Pen penSelected = new Pen(Color.Yellow, 2);
            graphics.DrawRectangle(penSelected, jSelected * imageWidth, iSelected * imageHeight, imageWidth, imageHeight);
        }
    }
}
