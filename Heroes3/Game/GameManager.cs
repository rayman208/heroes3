﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Tools;

namespace Heroes3.Game
{
    class GameManager
    {
        private static GameManager instance = null;

        private GameManager()
        {
            IniWorker.LoadParametes();
        }

        public static GameManager GetInstance()
        {
            if (instance == null)
            {
                instance = new GameManager();
            }

            return instance;
        }

        private BattleField battleField;
        private Player playerLeft;
        private Player playerRight;

        public void CreatePlayerLeft(Player player)
        {
            playerLeft = player;
        }

        public void CreatePlayerRight(Player player)
        {
            playerRight = player;
        }

        public void CreateBattleField(BattleField field)
        {
            battleField = field;
        }

        public void Draw(Graphics graphics)
        {
            graphics.Clear(Color.Empty);

            battleField.Draw(graphics);

            playerLeft.DrawUnits(graphics);
            playerRight.DrawUnits(graphics);
            
            battleField.DrawSelected(graphics);
        }
        
        public Player PlayerLeft
        {
            get { return playerLeft; }
        }

        public Player PlayerRight
        {
            get { return playerRight; }
        }

        public BattleField BattleField
        {
            get { return battleField; }
        }

    }
}
