﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Heroes3.Tools
{
    class IniWorker
    {
        private static Dictionary<string, string> parameters = new Dictionary<string, string>();

        public static void LoadParametes()
        {
            StreamReader reader = new StreamReader("units.ini");

            parameters.Clear();

            while (reader.EndOfStream == false)
            {
                string currentString = reader.ReadLine();
                string[] pair = currentString.Split('=');

                parameters.Add(pair[0], pair[1]);
            }

            reader.Close();
        }

        public static int GetIntParam(string key)
        {
            return int.Parse(parameters[key]);
        }

        public static string GetStringParam(string key)
        {
            return parameters[key];
        }
    }
}
