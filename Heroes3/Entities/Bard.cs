﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Tools;

namespace Heroes3.Entities
{
    class Bard : Unit
    {
        public Bard(int i, int j) : base(i, j, 
            IniWorker.GetIntParam("bard_initialHp"),
            IniWorker.GetIntParam("bard_initialDamage"),
            IniWorker.GetIntParam("bard_moveDistance"), 
            IniWorker.GetIntParam("bard_shootDistance"),
            Image.FromFile(IniWorker.GetStringParam("bard_image")),
            IniWorker.GetIntParam("bard_imageWidth"), 
            IniWorker.GetIntParam("bard_imageHeight"),
            IniWorker.GetIntParam("bard_cost"),
            IniWorker.GetStringParam("bard_name"))
        {
        }

        public static string GetInfo()
        {
            return IniWorker.GetStringParam("bard_name")+
                   $"\nначальное здоровье: {IniWorker.GetIntParam("bard_initialHp")}\n" +
                   $"начальный урон: {IniWorker.GetIntParam("bard_initialDamage")}\n" +
                   $"дальность ходьбы: {IniWorker.GetIntParam("bard_moveDistance")}\n" +
                   $"дальность атаки: {IniWorker.GetIntParam("bard_shootDistance")}\n" +
                   $"стоимость: {IniWorker.GetIntParam("bard_cost")}";
        }

        public static Image GetImage()
        {
            return Image.FromFile(IniWorker.GetStringParam("bard_image"));
        }

        public static string GetName()
        {
            return IniWorker.GetStringParam("bard_name");
        }
    }
}
