﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Heroes3.Entities
{
    class Unit
    {
        private int hp, initialHp, maxHp;
        private int damage, initialDamage;

        private int i, j;
        private int moveDistance, shootDistance;

        private Image image;
        private int imageWidth, imageHeight;

        private int cost;

        private string name;

        private bool isSteped;

        protected Unit(int i, int j, int initialHp, int initialDamage, int moveDistance, int shootDistance, Image image, int imageWidth, int imageHeight, int cost, string name)
        {
            this.initialHp = initialHp;
            this.initialDamage = initialDamage;

            this.hp = this.maxHp = this.initialHp;
            this.damage = this.initialDamage;

            this.i = i;
            this.j = j;
            
            this.moveDistance = moveDistance;
            this.shootDistance = shootDistance;

            this.image = image;
            this.imageWidth = imageWidth;
            this.imageHeight = imageHeight;

            this.cost = cost;

            this.name = name;

            this.isSteped = false;
        }

        public Unit GetCopy()
        {
            return (Unit)this.MemberwiseClone();
        }

        public override string ToString()
        {
            return $"{name} I:{i} J:{j} HP:{hp} Damage:{damage}";
        }

        public bool IsSteped
        {
            get { return isSteped; }
        }

        public int Cost
        {
            get { return cost; }
        }

        public int Hp
        {
            get { return hp; }
        }

        public int MaxHp
        {
            get { return maxHp; }
        }

        public int Damage
        {
            get { return damage; }
        }

        public int I
        {
            get { return i; }
        }

        public int J
        {
            get { return j; }
        }

        public void Draw(Graphics graphics, bool inverseImage)
        {
            if (inverseImage == false)
            {
                graphics.DrawImage(image, j * imageWidth, i * imageHeight, imageWidth, imageHeight);

                Pen penSelected = new Pen(Color.Red, 2);
                graphics.DrawRectangle(penSelected, j * imageWidth, i * imageHeight, imageWidth, imageHeight);
            }
            else
            {
                graphics.DrawImage(image, (j + 1) * imageWidth, i * imageHeight, -imageWidth, imageHeight);

                Pen penSelected = new Pen(Color.Blue, 2);
                graphics.DrawRectangle(penSelected, j * imageWidth, i * imageHeight, imageWidth, imageHeight);
            }

            if (isSteped == true)
            {
                Brush brush = new SolidBrush(Color.FromArgb(125, 128, 128, 128));

                graphics.FillRectangle(brush, j * imageWidth, i * imageHeight, imageWidth, imageHeight);
            }

            //todo отрисовка координат и хакактеристик юнита над юнитов
        }

        public void DrawMoveShootZone(Graphics graphics)
        {
            Pen penMove = new Pen(Color.GreenYellow, 2);
            Pen penShoot = new Pen(Color.DarkOrchid, 2);

            int moveDistance = this.moveDistance * 2;
            int shootDistance = this.shootDistance * 2;

            graphics.DrawEllipse(penMove,
                j * imageWidth - (imageWidth * moveDistance) / 2 + imageWidth / 2,
                i * imageHeight - (imageHeight * moveDistance) / 2 + imageHeight / 2,
                imageWidth * moveDistance,
                imageHeight * moveDistance);

            graphics.DrawEllipse(penShoot,
                j * imageWidth - (imageWidth * shootDistance) / 2 + imageWidth / 2,
                i * imageHeight - (imageHeight * shootDistance) / 2 + imageHeight / 2,
                imageWidth * shootDistance,
                imageHeight * shootDistance);
        }
        private int CalculateDistance(int i1, int j1, int i2, int j2)
        {
            return (int)Math.Sqrt(Math.Pow(i1 - i2, 2) + Math.Pow(j1 - j2, 2));
        }

        public bool Move(int newI, int newJ)
        {
            /*int cd = CalculateDistance(i, j, newI, newJ);
            Console.WriteLine($"cd = {cd}    moveDistance = {moveDistance}");*/
            if (CalculateDistance(i, j, newI, newJ) <= moveDistance)
            {
                i = newI;
                j = newJ;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Shoot(Unit unit)
        {
            /*int cd = CalculateDistance(i, j, newI, newJ);
            Console.WriteLine($"cd = {cd}    moveDistance = {moveDistance}");*/
            if (CalculateDistance(i, j, unit.i, unit.j) <= shootDistance)
            {
                unit.hp -= this.damage;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void MakeStep()
        {
            isSteped = true;
        }

        public void ResetStep()
        {
            isSteped = false;
        }
        
        protected void SetHpBuff(int percent)
        {
            int buff = initialHp * percent / 100;
            maxHp += buff;
            hp += buff;
        }

        protected void SetDamageBuff(int percent)
        {
            damage += initialDamage * percent / 100;
        }

        protected void Heal(int points)
        {
            hp += points;
            if (hp > maxHp)
            {
                hp = maxHp;
            }
        }
    }
}
