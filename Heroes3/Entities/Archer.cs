﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Tools;

namespace Heroes3.Entities
{
    class Archer : Unit
    {
        public Archer(int i, int j) : base(i, j,
            IniWorker.GetIntParam("archer_initialHp"),
            IniWorker.GetIntParam("archer_initialDamage"),
            IniWorker.GetIntParam("archer_moveDistance"),
            IniWorker.GetIntParam("archer_shootDistance"),
            Image.FromFile(IniWorker.GetStringParam("archer_image")),
            IniWorker.GetIntParam("archer_imageWidth"),
            IniWorker.GetIntParam("archer_imageHeight"),
            IniWorker.GetIntParam("archer_cost"),
            IniWorker.GetStringParam("archer_name"))
        {
        }

        public static string GetInfo()
        {
            return IniWorker.GetStringParam("archer_name") +
                   $"\nначальное здоровье: {IniWorker.GetIntParam("archer_initialHp")}\n" +
                   $"начальный урон: {IniWorker.GetIntParam("archer_initialDamage")}\n" +
                   $"дальность ходьбы: {IniWorker.GetIntParam("archer_moveDistance")}\n" +
                   $"дальность атаки: {IniWorker.GetIntParam("archer_shootDistance")}\n" +
                   $"стоимость: {IniWorker.GetIntParam("archer_cost")}";
        }

        public static Image GetImage()
        {
            return Image.FromFile(IniWorker.GetStringParam("archer_image"));
        }

        public static string GetName()
        {
            return IniWorker.GetStringParam("archer_name");
        }
        
    }
}
