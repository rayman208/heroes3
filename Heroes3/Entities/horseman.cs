﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Tools;

namespace Heroes3.Entities
{
    class Horseman : Unit
    {
        public Horseman(int i, int j) : base(i, j,
            IniWorker.GetIntParam("horseman_initialHp"), 
            IniWorker.GetIntParam("horseman_initialDamage"),
            IniWorker.GetIntParam("horseman_moveDistance"),
            IniWorker.GetIntParam("horseman_shootDistance"),
            Image.FromFile(IniWorker.GetStringParam("horseman_image")),
            IniWorker.GetIntParam("horseman_imageWidth"), 
            IniWorker.GetIntParam("horseman_imageHeight"), 
            IniWorker.GetIntParam("horseman_cost"),
            IniWorker.GetStringParam("horseman_name"))
        {
        }

        public static string GetInfo()
        {
            return IniWorker.GetStringParam("horseman_name")+
                   $"\nначальное здоровье: {IniWorker.GetIntParam("horseman_initialHp")}\n" +
                   $"начальный урон: {IniWorker.GetIntParam("horseman_initialDamage")}\n" +
                   $"дальность ходьбы: {IniWorker.GetIntParam("horseman_moveDistance")}\n" +
                   $"дальность атаки: {IniWorker.GetIntParam("horseman_shootDistance")}\n" +
                   $"стоимость: {IniWorker.GetIntParam("horseman_cost")}";
        }

        public static Image GetImage()
        {
            return Image.FromFile(IniWorker.GetStringParam("horseman_image"));
        }

        public static string GetName()
        {
            return IniWorker.GetStringParam("horseman_name");
        }
    }
}
