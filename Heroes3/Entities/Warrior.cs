﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Tools;

namespace Heroes3.Entities
{
    class Warrior:Unit
    {
        public Warrior(int i, int j) : base(i, j, 
            IniWorker.GetIntParam("warrior_initialHp"), 
            IniWorker.GetIntParam("warrior_initialDamage"),
            IniWorker.GetIntParam("warrior_moveDistance"), 
            IniWorker.GetIntParam("warrior_shootDistance"),
            Image.FromFile(IniWorker.GetStringParam("warrior_image")),
            IniWorker.GetIntParam("warrior_imageWidth"),
            IniWorker.GetIntParam("warrior_imageHeight"),
            IniWorker.GetIntParam("warrior_cost"),
            IniWorker.GetStringParam("warrior_name"))
        {
        }

        public static string GetInfo()
        {
            return IniWorker.GetStringParam("warrior_name")+
                   $"\nначальное здоровье: {IniWorker.GetIntParam("warrior_initialHp")}\n" +
                   $"начальный урон: {IniWorker.GetIntParam("warrior_initialDamage")}\n" +
                   $"дальность ходьбы: {IniWorker.GetIntParam("warrior_moveDistance")}\n" +
                   $"дальность атаки: {IniWorker.GetIntParam("warrior_shootDistance")}\n" +
                   $"стоимость: {IniWorker.GetIntParam("warrior_cost")}";
        }

        public static Image GetImage()
        {
            return Image.FromFile(IniWorker.GetStringParam("warrior_image"));
        }

        public static string GetName()
        {
            return IniWorker.GetStringParam("warrior_name");
        }

    }
}
