﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heroes3.Tools;

namespace Heroes3.Entities
{
    class Wizard : Unit
    {
        public Wizard(int i, int j) : base(i, j,
            IniWorker.GetIntParam("wizard_initialHp"),
            IniWorker.GetIntParam("wizard_initialDamage"),
            IniWorker.GetIntParam("wizard_moveDistance"),
            IniWorker.GetIntParam("wizard_shootDistance"),
            Image.FromFile(IniWorker.GetStringParam("wizard_image")),
            IniWorker.GetIntParam("wizard_imageWidth"),
            IniWorker.GetIntParam("wizard_imageHeight"),
            IniWorker.GetIntParam("wizard_cost"),
            IniWorker.GetStringParam("wizard_name"))
        {
        }

        public static string GetInfo()
        {
            return IniWorker.GetStringParam("wizard_name")+
                   $"\nначальное здоровье: {IniWorker.GetIntParam("wizard_initialHp")}\n" +
                   $"начальный урон: {IniWorker.GetIntParam("wizard_initialDamage")}\n" +
                   $"дальность ходьбы: {IniWorker.GetIntParam("wizard_moveDistance")}\n" +
                   $"дальность атаки: {IniWorker.GetIntParam("wizard_shootDistance")}\n" +
                   $"стоимость: {IniWorker.GetIntParam("wizard_cost")}";
        }

        public static Image GetImage()
        {
            return Image.FromFile(IniWorker.GetStringParam("wizard_image"));
        }

        public static string GetName()
        {
            return IniWorker.GetStringParam("wizard_name");
        }
    }
}
