﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Heroes3.Entities;
using Heroes3.Game;

namespace Heroes3.Forms
{
    public partial class FormShop : Form
    {
        private GameManager gameManager;
        private Bitmap bitmap;
        private Graphics graphics;

        private void BuyUnitForPlayerLeft(string unitName)
        {
            if (gameManager.BattleField.IsCellSelected == false)
            {
                MessageBox.Show("Ошибка. Выберите ячейку");
                return;
            }

            int i = gameManager.BattleField.ISelected;
            int j = gameManager.BattleField.JSelected;

            Unit uLeft = gameManager.PlayerLeft.GetUnitByIJ(i, j);
            Unit uRight = gameManager.PlayerRight.GetUnitByIJ(i, j);

            if (uLeft != null || uRight != null)
            {
                MessageBox.Show("Ошибка. Ячейка уже занята");
                return;
            }

            Unit buyUnit = null;

            if (unitName == Archer.GetName())
            {
                buyUnit = new Archer(i,j);
            }
            else if (unitName == Bard.GetName())
            {
                buyUnit = new Bard(i, j);
            }
            else if (unitName == Horseman.GetName())
            {
                buyUnit = new Horseman(i, j);
            }
            else if (unitName == Warrior.GetName())
            {
                buyUnit = new Warrior(i, j);
            }
            else if (unitName == Wizard.GetName())
            {
                buyUnit = new Wizard(i, j);
            }

            if (gameManager.PlayerLeft.BuyUnit(buyUnit) == false)
            {
                MessageBox.Show("Ошибка. У вас недостаточно денег на покупку");
                return;
            }

            labelMoneyLeft.Text = gameManager.PlayerLeft.Money.ToString();

            listBoxbuyUnitsLeft.DataSource = null;
            listBoxbuyUnitsLeft.DataSource = gameManager.PlayerLeft.Units;

            gameManager.BattleField.ResetSelectedCell();

            gameManager.Draw(graphics);
            pictureBoxBattleField.Invalidate();
        }

        private void BuyUnitForPlayerRight(string unitName)
        {
            if (gameManager.BattleField.IsCellSelected == false)
            {
                MessageBox.Show("Ошибка. Выберите ячейку");
                return;
            }

            int i = gameManager.BattleField.ISelected;
            int j = gameManager.BattleField.JSelected;

            Unit uLeft = gameManager.PlayerLeft.GetUnitByIJ(i, j);
            Unit uRight = gameManager.PlayerRight.GetUnitByIJ(i, j);

            if (uLeft != null || uRight != null)
            {
                MessageBox.Show("Ошибка. Ячейка уже занята");
                return;
            }

            Unit buyUnit = null;

            if (unitName == Archer.GetName())
            {
                buyUnit = new Archer(i, j);
            }
            else if (unitName == Bard.GetName())
            {
                buyUnit = new Bard(i, j);
            }
            else if (unitName == Horseman.GetName())
            {
                buyUnit = new Horseman(i, j);
            }
            else if (unitName == Warrior.GetName())
            {
                buyUnit = new Warrior(i, j);
            }
            else if (unitName == Wizard.GetName())
            {
                buyUnit = new Wizard(i, j);
            }

            if (gameManager.PlayerRight.BuyUnit(buyUnit) == false)
            {
                MessageBox.Show("Ошибка. У вас недостаточно денег на покупку");
                return;
            }

            labelMoneyRight.Text = gameManager.PlayerRight.Money.ToString();

            listBoxbuyUnitsRight.DataSource = null;
            listBoxbuyUnitsRight.DataSource = gameManager.PlayerRight.Units;

            gameManager.BattleField.ResetSelectedCell();

            gameManager.Draw(graphics);
            pictureBoxBattleField.Invalidate();
        }

        private void SellUnit(Player player)
        {
            if (gameManager.BattleField.IsCellSelected == false)
            {
                MessageBox.Show("Ошибка. Выберите ячейку");
                return;
            }

            int i = gameManager.BattleField.ISelected;
            int j = gameManager.BattleField.JSelected;

            Unit u = player.GetUnitByIJ(i, j);

            if (u == null)
            {
                MessageBox.Show("Ошибка. Существо не выбрано");
                return;
            }

            player.SellUnit(u);
        }


        public FormShop()
        {
            InitializeComponent();
        }

        private void FormShop_Load(object sender, EventArgs e)
        {
            gameManager = GameManager.GetInstance();

            labelNameLeft.Text = gameManager.PlayerLeft.Name;
            labelMoneyLeft.Text = gameManager.PlayerLeft.Money.ToString();

            labelNameRight.Text = gameManager.PlayerRight.Name;
            labelMoneyRight.Text = gameManager.PlayerRight.Money.ToString();

            labelFieldSize.Text = $"Rows: {gameManager.BattleField.Rows} Columns: {gameManager.BattleField.Columns}";

            labelIJSelected.Text = $"I:{gameManager.BattleField.ISelected} J:{gameManager.BattleField.JSelected}";

            labelIJSelected.Left = labelFieldSize.Left + labelFieldSize.Width + 5;

            pictureBoxBattleField.Left = 0;
            pictureBoxBattleField.Top = 0;
            pictureBoxBattleField.Width = gameManager.BattleField.Width;
            pictureBoxBattleField.Height = gameManager.BattleField.Height;
            
            panelBattleField.Width = pictureBoxBattleField.Width;
            panelBattleField.Height = pictureBoxBattleField.Height;

            panelPlayerRight.Left = panelBattleField.Left + panelBattleField.Width + 5;

            panelInfo.Top = panelBattleField.Top + panelBattleField.Height + 5;
            panelInfo.Width = panelBattleField.Width;

            buttonSaveAndContinue.Left = panelInfo.Width - buttonSaveAndContinue.Width - 5;
            
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.AutoSize = true;

            this.CenterToScreen();

            bitmap = new Bitmap(pictureBoxBattleField.Width, pictureBoxBattleField.Height);
            graphics = Graphics.FromImage(bitmap);

            pictureBoxBattleField.Image = bitmap;

            //gameManager.BattleField.Draw(graphics);
            gameManager.Draw(graphics);
            pictureBoxBattleField.Invalidate();
        }

        private void buttonInfoArcherLeft_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Archer.GetInfo(), Archer.GetImage()).ShowDialog();
        }

        private void buttonInfoArcherRight_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Archer.GetInfo(), Archer.GetImage()).ShowDialog();
        }

        private void buttonInfoBardLeft_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Bard.GetInfo(), Bard.GetImage()).ShowDialog();
        }

        private void buttonInfoBardRight_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Bard.GetInfo(), Bard.GetImage()).ShowDialog();
        }

        private void buttonInfoHorsemanLeft_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Horseman.GetInfo(), Horseman.GetImage()).ShowDialog();
        }

        private void buttonInfoHorsemanRight_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Horseman.GetInfo(), Horseman.GetImage()).ShowDialog();
        }

        private void buttonInfoWarriorLeft_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Warrior.GetInfo(), Warrior.GetImage()).ShowDialog();
        }

        private void buttonInfoWarriorRight_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Warrior.GetInfo(), Warrior.GetImage()).ShowDialog();
        }

        private void buttonInfoWizardLeft_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Wizard.GetInfo(), Wizard.GetImage()).ShowDialog();
        }

        private void buttonInfoWizardRight_Click(object sender, EventArgs e)
        {
            new FormUnitInfo(Wizard.GetInfo(), Wizard.GetImage()).ShowDialog();
        }

        private void FormShop_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void pictureBoxBattleField_MouseDown(object sender, MouseEventArgs e)
        {
            gameManager.BattleField.SetSelectedCell(e.X, e.Y);

            //gameManager.BattleField.Draw(graphics);
            gameManager.Draw(graphics);

            pictureBoxBattleField.Invalidate();

            labelIJSelected.Text = $"I:{gameManager.BattleField.ISelected} J:{gameManager.BattleField.JSelected}";

        }

        private void buttonbuyArcherLeft_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerLeft(Archer.GetName());
        }

        private void buttonbuyBardLeft_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerLeft(Bard.GetName());
        }

        private void buttonbuyHorsemanLeft_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerLeft(Horseman.GetName());
        }

        private void buttonbuyWarriorLeft_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerLeft(Warrior.GetName());
        }

        private void buttonbuyWizardLeft_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerLeft(Wizard.GetName());
        }

        private void buttonbuyArcherRight_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerRight(Archer.GetName());
        }

        private void buttonbuyBardRight_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerRight(Bard.GetName());
        }

        private void buttonbuyHorsemanRight_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerRight(Horseman.GetName());
        }

        private void buttonbuyWarriorRight_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerRight(Warrior.GetName());
        }

        private void buttonbuyWizardRight_Click(object sender, EventArgs e)
        {
            BuyUnitForPlayerRight(Wizard.GetName());
        }

        private void buttonSellUnitLeft_Click(object sender, EventArgs e)
        {
            SellUnit(gameManager.PlayerLeft);

            labelMoneyLeft.Text = gameManager.PlayerLeft.Money.ToString();

            listBoxbuyUnitsLeft.DataSource = null;
            listBoxbuyUnitsLeft.DataSource = gameManager.PlayerLeft.Units;

            gameManager.BattleField.ResetSelectedCell();

            gameManager.Draw(graphics);
            pictureBoxBattleField.Invalidate();
        }

        private void buttonSellUniRight_Click(object sender, EventArgs e)
        {  
            SellUnit(gameManager.PlayerRight);
            
            labelMoneyRight.Text = gameManager.PlayerRight.Money.ToString();

            listBoxbuyUnitsRight.DataSource = null;
            listBoxbuyUnitsRight.DataSource = gameManager.PlayerRight.Units;

            gameManager.BattleField.ResetSelectedCell();

            gameManager.Draw(graphics);
            pictureBoxBattleField.Invalidate();
        }

        private void buttonSaveAndContinue_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FormBattle().Show();
        }
    }
}
