﻿namespace Heroes3.Forms
{
    partial class FormBattle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxbuyUnitsRight = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labelNameRight = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.labelSelectedUnitInfo = new System.Windows.Forms.Label();
            this.labelIJSelected = new System.Windows.Forms.Label();
            this.labelFieldSize = new System.Windows.Forms.Label();
            this.panelPlayerRight = new System.Windows.Forms.Panel();
            this.panelPlayerLeft = new System.Windows.Forms.Panel();
            this.buttonMoveUnitLeft = new System.Windows.Forms.Button();
            this.listBoxbuyUnitsLeft = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelNameLeft = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelBattleField = new System.Windows.Forms.Panel();
            this.pictureBoxBattleField = new System.Windows.Forms.PictureBox();
            this.panelGameLog = new System.Windows.Forms.Panel();
            this.richTextBoxGameLog = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonShootUnitLeft = new System.Windows.Forms.Button();
            this.buttonNewRound = new System.Windows.Forms.Button();
            this.panelInfo.SuspendLayout();
            this.panelPlayerRight.SuspendLayout();
            this.panelPlayerLeft.SuspendLayout();
            this.panelBattleField.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBattleField)).BeginInit();
            this.panelGameLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxbuyUnitsRight
            // 
            this.listBoxbuyUnitsRight.FormattingEnabled = true;
            this.listBoxbuyUnitsRight.Location = new System.Drawing.Point(21, 228);
            this.listBoxbuyUnitsRight.Name = "listBoxbuyUnitsRight";
            this.listBoxbuyUnitsRight.Size = new System.Drawing.Size(220, 394);
            this.listBoxbuyUnitsRight.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(16, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 25);
            this.label6.TabIndex = 22;
            this.label6.Text = "Список существ";
            // 
            // labelNameRight
            // 
            this.labelNameRight.AutoSize = true;
            this.labelNameRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNameRight.Location = new System.Drawing.Point(16, 49);
            this.labelNameRight.Name = "labelNameRight";
            this.labelNameRight.Size = new System.Drawing.Size(53, 25);
            this.labelNameRight.TabIndex = 20;
            this.labelNameRight.Text = "Имя";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(16, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 25);
            this.label9.TabIndex = 19;
            this.label9.Text = "Игрок 2";
            // 
            // panelInfo
            // 
            this.panelInfo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelInfo.Controls.Add(this.buttonNewRound);
            this.panelInfo.Controls.Add(this.labelSelectedUnitInfo);
            this.panelInfo.Controls.Add(this.labelIJSelected);
            this.panelInfo.Controls.Add(this.labelFieldSize);
            this.panelInfo.Location = new System.Drawing.Point(274, 316);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(256, 85);
            this.panelInfo.TabIndex = 7;
            // 
            // labelSelectedUnitInfo
            // 
            this.labelSelectedUnitInfo.AutoSize = true;
            this.labelSelectedUnitInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelectedUnitInfo.Location = new System.Drawing.Point(6, 51);
            this.labelSelectedUnitInfo.Name = "labelSelectedUnitInfo";
            this.labelSelectedUnitInfo.Size = new System.Drawing.Size(233, 25);
            this.labelSelectedUnitInfo.TabIndex = 8;
            this.labelSelectedUnitInfo.Text = "Существо не выбрано";
            // 
            // labelIJSelected
            // 
            this.labelIJSelected.AutoSize = true;
            this.labelIJSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIJSelected.Location = new System.Drawing.Point(69, 17);
            this.labelIJSelected.Name = "labelIJSelected";
            this.labelIJSelected.Size = new System.Drawing.Size(57, 25);
            this.labelIJSelected.TabIndex = 7;
            this.labelIJSelected.Text = "fsize";
            // 
            // labelFieldSize
            // 
            this.labelFieldSize.AutoSize = true;
            this.labelFieldSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFieldSize.Location = new System.Drawing.Point(6, 17);
            this.labelFieldSize.Name = "labelFieldSize";
            this.labelFieldSize.Size = new System.Drawing.Size(57, 25);
            this.labelFieldSize.TabIndex = 5;
            this.labelFieldSize.Text = "fsize";
            // 
            // panelPlayerRight
            // 
            this.panelPlayerRight.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelPlayerRight.Controls.Add(this.listBoxbuyUnitsRight);
            this.panelPlayerRight.Controls.Add(this.label6);
            this.panelPlayerRight.Controls.Add(this.labelNameRight);
            this.panelPlayerRight.Controls.Add(this.label9);
            this.panelPlayerRight.Location = new System.Drawing.Point(536, 12);
            this.panelPlayerRight.Name = "panelPlayerRight";
            this.panelPlayerRight.Size = new System.Drawing.Size(270, 636);
            this.panelPlayerRight.TabIndex = 6;
            // 
            // panelPlayerLeft
            // 
            this.panelPlayerLeft.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelPlayerLeft.Controls.Add(this.buttonShootUnitLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonMoveUnitLeft);
            this.panelPlayerLeft.Controls.Add(this.listBoxbuyUnitsLeft);
            this.panelPlayerLeft.Controls.Add(this.label4);
            this.panelPlayerLeft.Controls.Add(this.labelNameLeft);
            this.panelPlayerLeft.Controls.Add(this.label1);
            this.panelPlayerLeft.Location = new System.Drawing.Point(12, 12);
            this.panelPlayerLeft.Name = "panelPlayerLeft";
            this.panelPlayerLeft.Size = new System.Drawing.Size(256, 636);
            this.panelPlayerLeft.TabIndex = 4;
            // 
            // buttonMoveUnitLeft
            // 
            this.buttonMoveUnitLeft.Location = new System.Drawing.Point(8, 88);
            this.buttonMoveUnitLeft.Name = "buttonMoveUnitLeft";
            this.buttonMoveUnitLeft.Size = new System.Drawing.Size(220, 23);
            this.buttonMoveUnitLeft.TabIndex = 5;
            this.buttonMoveUnitLeft.Text = "Переместить существо";
            this.buttonMoveUnitLeft.UseVisualStyleBackColor = true;
            this.buttonMoveUnitLeft.Click += new System.EventHandler(this.buttonMoveUnitLeft_Click);
            // 
            // listBoxbuyUnitsLeft
            // 
            this.listBoxbuyUnitsLeft.FormattingEnabled = true;
            this.listBoxbuyUnitsLeft.Location = new System.Drawing.Point(8, 228);
            this.listBoxbuyUnitsLeft.Name = "listBoxbuyUnitsLeft";
            this.listBoxbuyUnitsLeft.Size = new System.Drawing.Size(220, 394);
            this.listBoxbuyUnitsLeft.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Список существ";
            // 
            // labelNameLeft
            // 
            this.labelNameLeft.AutoSize = true;
            this.labelNameLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNameLeft.Location = new System.Drawing.Point(3, 46);
            this.labelNameLeft.Name = "labelNameLeft";
            this.labelNameLeft.Size = new System.Drawing.Size(53, 25);
            this.labelNameLeft.TabIndex = 1;
            this.labelNameLeft.Text = "Имя";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Игрок 1";
            // 
            // panelBattleField
            // 
            this.panelBattleField.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelBattleField.Controls.Add(this.pictureBoxBattleField);
            this.panelBattleField.Location = new System.Drawing.Point(274, 12);
            this.panelBattleField.Name = "panelBattleField";
            this.panelBattleField.Size = new System.Drawing.Size(256, 298);
            this.panelBattleField.TabIndex = 5;
            // 
            // pictureBoxBattleField
            // 
            this.pictureBoxBattleField.Location = new System.Drawing.Point(11, 14);
            this.pictureBoxBattleField.Name = "pictureBoxBattleField";
            this.pictureBoxBattleField.Size = new System.Drawing.Size(225, 107);
            this.pictureBoxBattleField.TabIndex = 0;
            this.pictureBoxBattleField.TabStop = false;
            this.pictureBoxBattleField.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxBattleField_MouseDown);
            // 
            // panelGameLog
            // 
            this.panelGameLog.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelGameLog.Controls.Add(this.richTextBoxGameLog);
            this.panelGameLog.Controls.Add(this.label2);
            this.panelGameLog.Location = new System.Drawing.Point(274, 407);
            this.panelGameLog.Name = "panelGameLog";
            this.panelGameLog.Size = new System.Drawing.Size(256, 241);
            this.panelGameLog.TabIndex = 9;
            // 
            // richTextBoxGameLog
            // 
            this.richTextBoxGameLog.Location = new System.Drawing.Point(11, 39);
            this.richTextBoxGameLog.Name = "richTextBoxGameLog";
            this.richTextBoxGameLog.ReadOnly = true;
            this.richTextBoxGameLog.Size = new System.Drawing.Size(225, 96);
            this.richTextBoxGameLog.TabIndex = 9;
            this.richTextBoxGameLog.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(6, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "Лог игры";
            // 
            // buttonShootUnitLeft
            // 
            this.buttonShootUnitLeft.Location = new System.Drawing.Point(8, 118);
            this.buttonShootUnitLeft.Name = "buttonShootUnitLeft";
            this.buttonShootUnitLeft.Size = new System.Drawing.Size(220, 23);
            this.buttonShootUnitLeft.TabIndex = 6;
            this.buttonShootUnitLeft.Text = "Атаковать другое существо";
            this.buttonShootUnitLeft.UseVisualStyleBackColor = true;
            this.buttonShootUnitLeft.Click += new System.EventHandler(this.buttonShootUnitLeft_Click);
            // 
            // buttonNewRound
            // 
            this.buttonNewRound.Location = new System.Drawing.Point(132, 17);
            this.buttonNewRound.Name = "buttonNewRound";
            this.buttonNewRound.Size = new System.Drawing.Size(107, 23);
            this.buttonNewRound.TabIndex = 9;
            this.buttonNewRound.Text = "Новый раунд";
            this.buttonNewRound.UseVisualStyleBackColor = true;
            this.buttonNewRound.Click += new System.EventHandler(this.buttonNewRound_Click);
            // 
            // FormBattle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 652);
            this.Controls.Add(this.panelGameLog);
            this.Controls.Add(this.panelInfo);
            this.Controls.Add(this.panelPlayerRight);
            this.Controls.Add(this.panelPlayerLeft);
            this.Controls.Add(this.panelBattleField);
            this.Name = "FormBattle";
            this.Text = "FormBattle";
            this.Load += new System.EventHandler(this.FormBattle_Load);
            this.panelInfo.ResumeLayout(false);
            this.panelInfo.PerformLayout();
            this.panelPlayerRight.ResumeLayout(false);
            this.panelPlayerRight.PerformLayout();
            this.panelPlayerLeft.ResumeLayout(false);
            this.panelPlayerLeft.PerformLayout();
            this.panelBattleField.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBattleField)).EndInit();
            this.panelGameLog.ResumeLayout(false);
            this.panelGameLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxbuyUnitsRight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelNameRight;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Label labelIJSelected;
        private System.Windows.Forms.Label labelFieldSize;
        private System.Windows.Forms.Panel panelPlayerRight;
        private System.Windows.Forms.Panel panelPlayerLeft;
        private System.Windows.Forms.ListBox listBoxbuyUnitsLeft;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelNameLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelBattleField;
        private System.Windows.Forms.PictureBox pictureBoxBattleField;
        private System.Windows.Forms.Label labelSelectedUnitInfo;
        private System.Windows.Forms.Panel panelGameLog;
        private System.Windows.Forms.RichTextBox richTextBoxGameLog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonMoveUnitLeft;
        private System.Windows.Forms.Button buttonShootUnitLeft;
        private System.Windows.Forms.Button buttonNewRound;
    }
}