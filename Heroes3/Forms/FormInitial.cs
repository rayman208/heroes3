﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Heroes3.Game;

namespace Heroes3.Forms
{
    public partial class FormInitial : Form
    {
        private GameManager gameManager;

        public FormInitial()
        {
            InitializeComponent();
        }

        private void FormInitial_Load(object sender, EventArgs e)
        {
            gameManager = GameManager.GetInstance();
        }

        private void buttonSaveAndContinue_Click(object sender, EventArgs e)
        {
            string name1, name2;
            int money1, money2;
            int rows, columns;

            if (textBoxName1.Text.Length == 0)
            {
                MessageBox.Show("Ошибка. Заполните имя игрока 1");
                return;
            }
            name1 = textBoxName1.Text;

            if (textBoxName2.Text.Length == 0)
            {
                MessageBox.Show("Ошибка. Заполните имя игрока 2");
                return;
            }
            name2 = textBoxName2.Text;


            money1 = Convert.ToInt32(numericUpDownMoney1.Value);
            money2 = Convert.ToInt32(numericUpDownMoney2.Value);

            rows = Convert.ToInt32(numericUpDownRows.Value);
            columns = Convert.ToInt32(numericUpDownColumns.Value);

            gameManager.CreatePlayerLeft(new Player(name1, money1, false));
            gameManager.CreatePlayerRight(new Player(name2, money2, true));
            gameManager.CreateBattleField(new BattleField(rows,columns));

            this.Hide();
            new FormShop().Show();
        }
    }
}
