﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Heroes3.Entities;
using Heroes3.Game;
namespace Heroes3.Forms
{
    public partial class FormBattle : Form
    {
        private enum UnitActions
        {
            None,
            MoveLeft,
            ShootLeft,
            MoveRight,
            ShootRight
        }

        private enum ClickNumber
        {
            First,
            Second
        }

        private UnitActions currentAction;
        private Unit firstSelectedUnit;
        private ClickNumber currentClickNumber;

        private GameManager gameManager;
        private Bitmap bitmap;
        private Graphics graphics;

        public void UpdateListBoxbuyUnitsRight()
        {
            listBoxbuyUnitsRight.DataSource = null;
            listBoxbuyUnitsRight.DataSource = gameManager.PlayerRight.Units;
        }

        public void UpdateListBoxbuyUnitsLeft()
        {
            listBoxbuyUnitsLeft.DataSource = null;
            listBoxbuyUnitsLeft.DataSource = gameManager.PlayerLeft.Units;
        }


        public FormBattle()
        {
            InitializeComponent();
        }

        private void FormBattle_Load(object sender, EventArgs e)
        {
            gameManager = GameManager.GetInstance();
            currentAction = UnitActions.None;
            currentClickNumber = ClickNumber.First;
            firstSelectedUnit = null;

            labelNameLeft.Text = gameManager.PlayerLeft.Name;

            labelNameRight.Text = gameManager.PlayerRight.Name;
           
            labelFieldSize.Text = $"Rows: {gameManager.BattleField.Rows} Columns: {gameManager.BattleField.Columns}";

            labelIJSelected.Text = $"I:{gameManager.BattleField.ISelected} J:{gameManager.BattleField.JSelected}";

            labelIJSelected.Left = labelFieldSize.Left + labelFieldSize.Width + 5;

            pictureBoxBattleField.Left = 0;
            pictureBoxBattleField.Top = 0;
            pictureBoxBattleField.Width = gameManager.BattleField.Width;
            pictureBoxBattleField.Height = gameManager.BattleField.Height;

            panelBattleField.Width = pictureBoxBattleField.Width;
            panelBattleField.Height = pictureBoxBattleField.Height;

            panelPlayerRight.Left = panelBattleField.Left + panelBattleField.Width + 5;

            panelInfo.Top = panelBattleField.Top + panelBattleField.Height + 5;
            panelInfo.Width = panelBattleField.Width;

            buttonNewRound.Left = panelInfo.Width - buttonNewRound.Width - 5;

            panelGameLog.Top = panelInfo.Top + panelInfo.Height + 5;
            panelGameLog.Width = panelInfo.Width;

            panelGameLog.Height = this.Height - panelGameLog.Top - 42;

            richTextBoxGameLog.Width = panelGameLog.Width - 30;
            richTextBoxGameLog.Height = panelGameLog.Height - 50;


            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.AutoSize = true;

            this.CenterToScreen();

            bitmap = new Bitmap(pictureBoxBattleField.Width, pictureBoxBattleField.Height);
            graphics = Graphics.FromImage(bitmap);

            pictureBoxBattleField.Image = bitmap;

            //gameManager.BattleField.Draw(graphics);
            gameManager.Draw(graphics);
            pictureBoxBattleField.Invalidate();

            UpdateListBoxbuyUnitsLeft();
            UpdateListBoxbuyUnitsRight();
        }

        private void pictureBoxBattleField_MouseDown(object sender, MouseEventArgs e)
        {
            gameManager.BattleField.SetSelectedCell(e.X, e.Y);

            int i = gameManager.BattleField.ISelected;
            int j = gameManager.BattleField.JSelected;

            labelIJSelected.Text = $"I:{i} J:{j}";
            Unit uLeft = gameManager.PlayerLeft.GetUnitByIJ(i, j);
            Unit uRight = gameManager.PlayerRight.GetUnitByIJ(i, j);

            bool result;
            Unit secondSelectedUnit;

            switch (currentClickNumber)
            {
                case ClickNumber.First:

                    gameManager.Draw(graphics);

                    if (uLeft != null)
                    {
                        labelSelectedUnitInfo.Text = uLeft.ToString();
                        uLeft.DrawMoveShootZone(graphics);

                        firstSelectedUnit = uLeft;
                    }
                    else if (uRight != null)
                    {
                        labelSelectedUnitInfo.Text = uRight.ToString();
                        uRight.DrawMoveShootZone(graphics);

                        firstSelectedUnit = uRight;
                    }
                    else
                    {
                        labelSelectedUnitInfo.Text = "Существо не выбрано";
                        firstSelectedUnit = null;
                    }

                    break;
                case ClickNumber.Second:

                    switch (currentAction)
                    {
                        case UnitActions.MoveLeft:
                            if (uLeft != null || uRight != null)
                            {
                                MessageBox.Show("Ошибка. Данная клетка уже занята");
                                return;
                            }

                            result = firstSelectedUnit.Move(i, j);

                            if (result == false)
                            {
                                MessageBox.Show("Невозможно походить. клетка слишком далеко");
                                return;
                            }

                            richTextBoxGameLog.Text += $"I:{i} J:{j}" + "\n\n";

                            firstSelectedUnit.MakeStep();
                            firstSelectedUnit = null;
                            currentClickNumber = ClickNumber.First;

                            break;
                        case UnitActions.ShootLeft:

                            secondSelectedUnit = uRight;
                            
                            if (secondSelectedUnit == null)
                            {
                                MessageBox.Show("Ошибка. В данной клетке нет вражеского существа");
                                return;
                            }

                            Unit secondCopy = secondSelectedUnit.GetCopy();

                            result = firstSelectedUnit.Shoot(secondSelectedUnit);

                            if (result == false)
                            {
                                MessageBox.Show("Невозможно атаковать. Существо слишком далеко");
                                return;
                            }

                            richTextBoxGameLog.Text += secondCopy + $" на {firstSelectedUnit.Damage} едениц" + "\n\n";

                            gameManager.PlayerRight.DeleteDeadUnits();
                            UpdateListBoxbuyUnitsRight();

                            firstSelectedUnit.MakeStep();
                            firstSelectedUnit = null;
                            currentClickNumber = ClickNumber.First;
                            
                            break;
                        case UnitActions.MoveRight:
                            break;
                        case UnitActions.ShootRight:
                            break;
                    }

                    gameManager.Draw(graphics);

                    break;
            }


            pictureBoxBattleField.Invalidate();
        }

        private void buttonMoveUnitLeft_Click(object sender, EventArgs e)
        {
            if (firstSelectedUnit == null)
            {
                MessageBox.Show("Ошибка. Существо не выбрано");
                return;
            }

            if (gameManager.PlayerLeft.ContainsUnit(firstSelectedUnit) == false)
            {
                MessageBox.Show($"Ошибка. Данное существо не пренадлежит {gameManager.PlayerLeft.Name}");
                return;
            }

            if (firstSelectedUnit.IsSteped == true)
            {
                MessageBox.Show("Ошибка. Существо уже ходило, выберите другое существо");
                return;
            }

            currentAction = UnitActions.MoveLeft;
            currentClickNumber = ClickNumber.Second;
            MessageBox.Show("Теперь выберите клетку для новой позиции существа");

            richTextBoxGameLog.Text +=
                gameManager.PlayerLeft.Name + " перемещает " + firstSelectedUnit + " в координаты: ";
        }

        private void buttonShootUnitLeft_Click(object sender, EventArgs e)
        {
            if (firstSelectedUnit == null)
            {
                MessageBox.Show("Ошибка. Существо не выбрано");
                return;
            }

            if (gameManager.PlayerLeft.ContainsUnit(firstSelectedUnit) == false)
            {
                MessageBox.Show($"Ошибка. Данное существо не пренадлежит {gameManager.PlayerLeft.Name}");
                return;
            }

            if (firstSelectedUnit.IsSteped == true)
            {
                MessageBox.Show("Ошибка. Существо уже ходило, выберите другое существо");
                return;
            }

            currentAction = UnitActions.ShootLeft;
            currentClickNumber = ClickNumber.Second;
            MessageBox.Show("Теперь выберите атакуемое cущество");

            richTextBoxGameLog.Text +=
                gameManager.PlayerLeft.Name + " атакует существом " + firstSelectedUnit + " существо: ";
        }

        private void buttonNewRound_Click(object sender, EventArgs e)
        {
            gameManager.PlayerLeft.ResetUnitsStep();
            gameManager.PlayerRight.ResetUnitsStep();

            gameManager.Draw(graphics);
            pictureBoxBattleField.Invalidate();
        }
    }
}
