﻿namespace Heroes3.Forms
{
    partial class FormInitial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxName1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSaveAndContinue = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxName2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownRows = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownColumns = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMoney1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMoney2 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMoney1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMoney2)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxName1
            // 
            this.textBoxName1.Location = new System.Drawing.Point(43, 81);
            this.textBoxName1.Name = "textBoxName1";
            this.textBoxName1.Size = new System.Drawing.Size(100, 20);
            this.textBoxName1.TabIndex = 0;
            this.textBoxName1.Text = "Безымянный 1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя игрока 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Деньги игрока 1";
            // 
            // buttonSaveAndContinue
            // 
            this.buttonSaveAndContinue.Location = new System.Drawing.Point(43, 153);
            this.buttonSaveAndContinue.Name = "buttonSaveAndContinue";
            this.buttonSaveAndContinue.Size = new System.Drawing.Size(380, 23);
            this.buttonSaveAndContinue.TabIndex = 4;
            this.buttonSaveAndContinue.Text = "Сохранить и продолжить";
            this.buttonSaveAndContinue.UseVisualStyleBackColor = true;
            this.buttonSaveAndContinue.Click += new System.EventHandler(this.buttonSaveAndContinue_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(174, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Деньги игрока 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Имя игрока 2";
            // 
            // textBoxName2
            // 
            this.textBoxName2.Location = new System.Drawing.Point(174, 81);
            this.textBoxName2.Name = "textBoxName2";
            this.textBoxName2.Size = new System.Drawing.Size(100, 20);
            this.textBoxName2.TabIndex = 5;
            this.textBoxName2.Text = "Безымянный 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(305, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Кол-во строк поля";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(305, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Кол-во столбцов поля";
            // 
            // numericUpDownRows
            // 
            this.numericUpDownRows.Location = new System.Drawing.Point(308, 81);
            this.numericUpDownRows.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownRows.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownRows.Name = "numericUpDownRows";
            this.numericUpDownRows.Size = new System.Drawing.Size(115, 20);
            this.numericUpDownRows.TabIndex = 16;
            this.numericUpDownRows.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numericUpDownColumns
            // 
            this.numericUpDownColumns.Location = new System.Drawing.Point(308, 127);
            this.numericUpDownColumns.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.numericUpDownColumns.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownColumns.Name = "numericUpDownColumns";
            this.numericUpDownColumns.Size = new System.Drawing.Size(115, 20);
            this.numericUpDownColumns.TabIndex = 17;
            this.numericUpDownColumns.Value = new decimal(new int[] {
            22,
            0,
            0,
            0});
            // 
            // numericUpDownMoney1
            // 
            this.numericUpDownMoney1.Location = new System.Drawing.Point(43, 127);
            this.numericUpDownMoney1.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMoney1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownMoney1.Name = "numericUpDownMoney1";
            this.numericUpDownMoney1.Size = new System.Drawing.Size(100, 20);
            this.numericUpDownMoney1.TabIndex = 18;
            this.numericUpDownMoney1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numericUpDownMoney2
            // 
            this.numericUpDownMoney2.Location = new System.Drawing.Point(177, 127);
            this.numericUpDownMoney2.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownMoney2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownMoney2.Name = "numericUpDownMoney2";
            this.numericUpDownMoney2.Size = new System.Drawing.Size(97, 20);
            this.numericUpDownMoney2.TabIndex = 19;
            this.numericUpDownMoney2.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // FormInitial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 233);
            this.Controls.Add(this.numericUpDownMoney2);
            this.Controls.Add(this.numericUpDownMoney1);
            this.Controls.Add(this.numericUpDownColumns);
            this.Controls.Add(this.numericUpDownRows);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxName2);
            this.Controls.Add(this.buttonSaveAndContinue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxName1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInitial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormInitial";
            this.Load += new System.EventHandler(this.FormInitial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMoney1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMoney2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxName1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSaveAndContinue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxName2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDownRows;
        private System.Windows.Forms.NumericUpDown numericUpDownColumns;
        private System.Windows.Forms.NumericUpDown numericUpDownMoney1;
        private System.Windows.Forms.NumericUpDown numericUpDownMoney2;
    }
}