﻿namespace Heroes3.Forms
{
    partial class FormShop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelPlayerLeft = new System.Windows.Forms.Panel();
            this.buttonSellUnitLeft = new System.Windows.Forms.Button();
            this.buttonInfoWizardLeft = new System.Windows.Forms.Button();
            this.buttonbuyWizardLeft = new System.Windows.Forms.Button();
            this.buttonInfoWarriorLeft = new System.Windows.Forms.Button();
            this.buttonbuyWarriorLeft = new System.Windows.Forms.Button();
            this.buttonInfoHorsemanLeft = new System.Windows.Forms.Button();
            this.buttonbuyHorsemanLeft = new System.Windows.Forms.Button();
            this.buttonInfoBardLeft = new System.Windows.Forms.Button();
            this.buttonbuyBardLeft = new System.Windows.Forms.Button();
            this.buttonInfoArcherLeft = new System.Windows.Forms.Button();
            this.buttonbuyArcherLeft = new System.Windows.Forms.Button();
            this.listBoxbuyUnitsLeft = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelMoneyLeft = new System.Windows.Forms.Label();
            this.labelNameLeft = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelBattleField = new System.Windows.Forms.Panel();
            this.pictureBoxBattleField = new System.Windows.Forms.PictureBox();
            this.panelPlayerRight = new System.Windows.Forms.Panel();
            this.buttonSellUniRight = new System.Windows.Forms.Button();
            this.buttonInfoWizardRight = new System.Windows.Forms.Button();
            this.buttonbuyWizardRight = new System.Windows.Forms.Button();
            this.buttonInfoWarriorRight = new System.Windows.Forms.Button();
            this.buttonbuyWarriorRight = new System.Windows.Forms.Button();
            this.buttonInfoHorsemanRight = new System.Windows.Forms.Button();
            this.buttonbuyHorsemanRight = new System.Windows.Forms.Button();
            this.buttonInfoBardRight = new System.Windows.Forms.Button();
            this.buttonbuyBardRight = new System.Windows.Forms.Button();
            this.buttonInfoArcherRight = new System.Windows.Forms.Button();
            this.buttonbuyArcherRight = new System.Windows.Forms.Button();
            this.listBoxbuyUnitsRight = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labelMoneyRight = new System.Windows.Forms.Label();
            this.labelNameRight = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.labelIJSelected = new System.Windows.Forms.Label();
            this.buttonSaveAndContinue = new System.Windows.Forms.Button();
            this.labelFieldSize = new System.Windows.Forms.Label();
            this.panelPlayerLeft.SuspendLayout();
            this.panelBattleField.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBattleField)).BeginInit();
            this.panelPlayerRight.SuspendLayout();
            this.panelInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPlayerLeft
            // 
            this.panelPlayerLeft.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelPlayerLeft.Controls.Add(this.buttonSellUnitLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonInfoWizardLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonbuyWizardLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonInfoWarriorLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonbuyWarriorLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonInfoHorsemanLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonbuyHorsemanLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonInfoBardLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonbuyBardLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonInfoArcherLeft);
            this.panelPlayerLeft.Controls.Add(this.buttonbuyArcherLeft);
            this.panelPlayerLeft.Controls.Add(this.listBoxbuyUnitsLeft);
            this.panelPlayerLeft.Controls.Add(this.label4);
            this.panelPlayerLeft.Controls.Add(this.labelMoneyLeft);
            this.panelPlayerLeft.Controls.Add(this.labelNameLeft);
            this.panelPlayerLeft.Controls.Add(this.label1);
            this.panelPlayerLeft.Location = new System.Drawing.Point(12, 12);
            this.panelPlayerLeft.Name = "panelPlayerLeft";
            this.panelPlayerLeft.Size = new System.Drawing.Size(256, 636);
            this.panelPlayerLeft.TabIndex = 0;
            // 
            // buttonSellUnitLeft
            // 
            this.buttonSellUnitLeft.Location = new System.Drawing.Point(11, 260);
            this.buttonSellUnitLeft.Name = "buttonSellUnitLeft";
            this.buttonSellUnitLeft.Size = new System.Drawing.Size(217, 23);
            this.buttonSellUnitLeft.TabIndex = 19;
            this.buttonSellUnitLeft.Text = "Продать существо";
            this.buttonSellUnitLeft.UseVisualStyleBackColor = true;
            this.buttonSellUnitLeft.Click += new System.EventHandler(this.buttonSellUnitLeft_Click);
            // 
            // buttonInfoWizardLeft
            // 
            this.buttonInfoWizardLeft.Location = new System.Drawing.Point(11, 231);
            this.buttonInfoWizardLeft.Name = "buttonInfoWizardLeft";
            this.buttonInfoWizardLeft.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoWizardLeft.TabIndex = 18;
            this.buttonInfoWizardLeft.Text = "Инфо мага";
            this.buttonInfoWizardLeft.UseVisualStyleBackColor = true;
            this.buttonInfoWizardLeft.Click += new System.EventHandler(this.buttonInfoWizardLeft_Click);
            // 
            // buttonbuyWizardLeft
            // 
            this.buttonbuyWizardLeft.Location = new System.Drawing.Point(120, 231);
            this.buttonbuyWizardLeft.Name = "buttonbuyWizardLeft";
            this.buttonbuyWizardLeft.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyWizardLeft.TabIndex = 17;
            this.buttonbuyWizardLeft.Text = "Купить мага";
            this.buttonbuyWizardLeft.UseVisualStyleBackColor = true;
            this.buttonbuyWizardLeft.Click += new System.EventHandler(this.buttonbuyWizardLeft_Click);
            // 
            // buttonInfoWarriorLeft
            // 
            this.buttonInfoWarriorLeft.Location = new System.Drawing.Point(11, 202);
            this.buttonInfoWarriorLeft.Name = "buttonInfoWarriorLeft";
            this.buttonInfoWarriorLeft.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoWarriorLeft.TabIndex = 16;
            this.buttonInfoWarriorLeft.Text = "Инфо рыцаря";
            this.buttonInfoWarriorLeft.UseVisualStyleBackColor = true;
            this.buttonInfoWarriorLeft.Click += new System.EventHandler(this.buttonInfoWarriorLeft_Click);
            // 
            // buttonbuyWarriorLeft
            // 
            this.buttonbuyWarriorLeft.Location = new System.Drawing.Point(120, 202);
            this.buttonbuyWarriorLeft.Name = "buttonbuyWarriorLeft";
            this.buttonbuyWarriorLeft.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyWarriorLeft.TabIndex = 15;
            this.buttonbuyWarriorLeft.Text = "Купить рыцаря";
            this.buttonbuyWarriorLeft.UseVisualStyleBackColor = true;
            this.buttonbuyWarriorLeft.Click += new System.EventHandler(this.buttonbuyWarriorLeft_Click);
            // 
            // buttonInfoHorsemanLeft
            // 
            this.buttonInfoHorsemanLeft.Location = new System.Drawing.Point(11, 173);
            this.buttonInfoHorsemanLeft.Name = "buttonInfoHorsemanLeft";
            this.buttonInfoHorsemanLeft.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoHorsemanLeft.TabIndex = 14;
            this.buttonInfoHorsemanLeft.Text = "Инфо наездника";
            this.buttonInfoHorsemanLeft.UseVisualStyleBackColor = true;
            this.buttonInfoHorsemanLeft.Click += new System.EventHandler(this.buttonInfoHorsemanLeft_Click);
            // 
            // buttonbuyHorsemanLeft
            // 
            this.buttonbuyHorsemanLeft.Location = new System.Drawing.Point(120, 173);
            this.buttonbuyHorsemanLeft.Name = "buttonbuyHorsemanLeft";
            this.buttonbuyHorsemanLeft.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyHorsemanLeft.TabIndex = 13;
            this.buttonbuyHorsemanLeft.Text = "Купить наездника";
            this.buttonbuyHorsemanLeft.UseVisualStyleBackColor = true;
            this.buttonbuyHorsemanLeft.Click += new System.EventHandler(this.buttonbuyHorsemanLeft_Click);
            // 
            // buttonInfoBardLeft
            // 
            this.buttonInfoBardLeft.Location = new System.Drawing.Point(11, 144);
            this.buttonInfoBardLeft.Name = "buttonInfoBardLeft";
            this.buttonInfoBardLeft.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoBardLeft.TabIndex = 12;
            this.buttonInfoBardLeft.Text = "Инфо барда";
            this.buttonInfoBardLeft.UseVisualStyleBackColor = true;
            this.buttonInfoBardLeft.Click += new System.EventHandler(this.buttonInfoBardLeft_Click);
            // 
            // buttonbuyBardLeft
            // 
            this.buttonbuyBardLeft.Location = new System.Drawing.Point(120, 144);
            this.buttonbuyBardLeft.Name = "buttonbuyBardLeft";
            this.buttonbuyBardLeft.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyBardLeft.TabIndex = 11;
            this.buttonbuyBardLeft.Text = "Купить барда";
            this.buttonbuyBardLeft.UseVisualStyleBackColor = true;
            this.buttonbuyBardLeft.Click += new System.EventHandler(this.buttonbuyBardLeft_Click);
            // 
            // buttonInfoArcherLeft
            // 
            this.buttonInfoArcherLeft.Location = new System.Drawing.Point(11, 115);
            this.buttonInfoArcherLeft.Name = "buttonInfoArcherLeft";
            this.buttonInfoArcherLeft.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoArcherLeft.TabIndex = 10;
            this.buttonInfoArcherLeft.Text = "Инфо лучника";
            this.buttonInfoArcherLeft.UseVisualStyleBackColor = true;
            this.buttonInfoArcherLeft.Click += new System.EventHandler(this.buttonInfoArcherLeft_Click);
            // 
            // buttonbuyArcherLeft
            // 
            this.buttonbuyArcherLeft.Location = new System.Drawing.Point(120, 115);
            this.buttonbuyArcherLeft.Name = "buttonbuyArcherLeft";
            this.buttonbuyArcherLeft.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyArcherLeft.TabIndex = 5;
            this.buttonbuyArcherLeft.Text = "Купить лучника";
            this.buttonbuyArcherLeft.UseVisualStyleBackColor = true;
            this.buttonbuyArcherLeft.Click += new System.EventHandler(this.buttonbuyArcherLeft_Click);
            // 
            // listBoxbuyUnitsLeft
            // 
            this.listBoxbuyUnitsLeft.FormattingEnabled = true;
            this.listBoxbuyUnitsLeft.Location = new System.Drawing.Point(8, 381);
            this.listBoxbuyUnitsLeft.Name = "listBoxbuyUnitsLeft";
            this.listBoxbuyUnitsLeft.Size = new System.Drawing.Size(220, 238);
            this.listBoxbuyUnitsLeft.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 353);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(225, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Купленные существа";
            // 
            // labelMoneyLeft
            // 
            this.labelMoneyLeft.AutoSize = true;
            this.labelMoneyLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMoneyLeft.Location = new System.Drawing.Point(3, 71);
            this.labelMoneyLeft.Name = "labelMoneyLeft";
            this.labelMoneyLeft.Size = new System.Drawing.Size(83, 25);
            this.labelMoneyLeft.TabIndex = 2;
            this.labelMoneyLeft.Text = "Деньги";
            // 
            // labelNameLeft
            // 
            this.labelNameLeft.AutoSize = true;
            this.labelNameLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNameLeft.Location = new System.Drawing.Point(3, 46);
            this.labelNameLeft.Name = "labelNameLeft";
            this.labelNameLeft.Size = new System.Drawing.Size(53, 25);
            this.labelNameLeft.TabIndex = 1;
            this.labelNameLeft.Text = "Имя";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Игрок 1";
            // 
            // panelBattleField
            // 
            this.panelBattleField.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelBattleField.Controls.Add(this.pictureBoxBattleField);
            this.panelBattleField.Location = new System.Drawing.Point(274, 12);
            this.panelBattleField.Name = "panelBattleField";
            this.panelBattleField.Size = new System.Drawing.Size(256, 571);
            this.panelBattleField.TabIndex = 1;
            // 
            // pictureBoxBattleField
            // 
            this.pictureBoxBattleField.Location = new System.Drawing.Point(28, 31);
            this.pictureBoxBattleField.Name = "pictureBoxBattleField";
            this.pictureBoxBattleField.Size = new System.Drawing.Size(225, 107);
            this.pictureBoxBattleField.TabIndex = 0;
            this.pictureBoxBattleField.TabStop = false;
            this.pictureBoxBattleField.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxBattleField_MouseDown);
            // 
            // panelPlayerRight
            // 
            this.panelPlayerRight.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelPlayerRight.Controls.Add(this.buttonSellUniRight);
            this.panelPlayerRight.Controls.Add(this.buttonInfoWizardRight);
            this.panelPlayerRight.Controls.Add(this.buttonbuyWizardRight);
            this.panelPlayerRight.Controls.Add(this.buttonInfoWarriorRight);
            this.panelPlayerRight.Controls.Add(this.buttonbuyWarriorRight);
            this.panelPlayerRight.Controls.Add(this.buttonInfoHorsemanRight);
            this.panelPlayerRight.Controls.Add(this.buttonbuyHorsemanRight);
            this.panelPlayerRight.Controls.Add(this.buttonInfoBardRight);
            this.panelPlayerRight.Controls.Add(this.buttonbuyBardRight);
            this.panelPlayerRight.Controls.Add(this.buttonInfoArcherRight);
            this.panelPlayerRight.Controls.Add(this.buttonbuyArcherRight);
            this.panelPlayerRight.Controls.Add(this.listBoxbuyUnitsRight);
            this.panelPlayerRight.Controls.Add(this.label6);
            this.panelPlayerRight.Controls.Add(this.labelMoneyRight);
            this.panelPlayerRight.Controls.Add(this.labelNameRight);
            this.panelPlayerRight.Controls.Add(this.label9);
            this.panelPlayerRight.Location = new System.Drawing.Point(536, 12);
            this.panelPlayerRight.Name = "panelPlayerRight";
            this.panelPlayerRight.Size = new System.Drawing.Size(270, 636);
            this.panelPlayerRight.TabIndex = 2;
            // 
            // buttonSellUniRight
            // 
            this.buttonSellUniRight.Location = new System.Drawing.Point(24, 263);
            this.buttonSellUniRight.Name = "buttonSellUniRight";
            this.buttonSellUniRight.Size = new System.Drawing.Size(217, 23);
            this.buttonSellUniRight.TabIndex = 34;
            this.buttonSellUniRight.Text = "Продать существо";
            this.buttonSellUniRight.UseVisualStyleBackColor = true;
            this.buttonSellUniRight.Click += new System.EventHandler(this.buttonSellUniRight_Click);
            // 
            // buttonInfoWizardRight
            // 
            this.buttonInfoWizardRight.Location = new System.Drawing.Point(24, 234);
            this.buttonInfoWizardRight.Name = "buttonInfoWizardRight";
            this.buttonInfoWizardRight.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoWizardRight.TabIndex = 33;
            this.buttonInfoWizardRight.Text = "Инфо мага";
            this.buttonInfoWizardRight.UseVisualStyleBackColor = true;
            this.buttonInfoWizardRight.Click += new System.EventHandler(this.buttonInfoWizardRight_Click);
            // 
            // buttonbuyWizardRight
            // 
            this.buttonbuyWizardRight.Location = new System.Drawing.Point(133, 234);
            this.buttonbuyWizardRight.Name = "buttonbuyWizardRight";
            this.buttonbuyWizardRight.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyWizardRight.TabIndex = 32;
            this.buttonbuyWizardRight.Text = "Купить мага";
            this.buttonbuyWizardRight.UseVisualStyleBackColor = true;
            this.buttonbuyWizardRight.Click += new System.EventHandler(this.buttonbuyWizardRight_Click);
            // 
            // buttonInfoWarriorRight
            // 
            this.buttonInfoWarriorRight.Location = new System.Drawing.Point(24, 205);
            this.buttonInfoWarriorRight.Name = "buttonInfoWarriorRight";
            this.buttonInfoWarriorRight.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoWarriorRight.TabIndex = 31;
            this.buttonInfoWarriorRight.Text = "Инфо рыцаря";
            this.buttonInfoWarriorRight.UseVisualStyleBackColor = true;
            this.buttonInfoWarriorRight.Click += new System.EventHandler(this.buttonInfoWarriorRight_Click);
            // 
            // buttonbuyWarriorRight
            // 
            this.buttonbuyWarriorRight.Location = new System.Drawing.Point(133, 205);
            this.buttonbuyWarriorRight.Name = "buttonbuyWarriorRight";
            this.buttonbuyWarriorRight.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyWarriorRight.TabIndex = 30;
            this.buttonbuyWarriorRight.Text = "Купить рыцаря";
            this.buttonbuyWarriorRight.UseVisualStyleBackColor = true;
            this.buttonbuyWarriorRight.Click += new System.EventHandler(this.buttonbuyWarriorRight_Click);
            // 
            // buttonInfoHorsemanRight
            // 
            this.buttonInfoHorsemanRight.Location = new System.Drawing.Point(24, 176);
            this.buttonInfoHorsemanRight.Name = "buttonInfoHorsemanRight";
            this.buttonInfoHorsemanRight.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoHorsemanRight.TabIndex = 29;
            this.buttonInfoHorsemanRight.Text = "Инфо наездника";
            this.buttonInfoHorsemanRight.UseVisualStyleBackColor = true;
            this.buttonInfoHorsemanRight.Click += new System.EventHandler(this.buttonInfoHorsemanRight_Click);
            // 
            // buttonbuyHorsemanRight
            // 
            this.buttonbuyHorsemanRight.Location = new System.Drawing.Point(133, 176);
            this.buttonbuyHorsemanRight.Name = "buttonbuyHorsemanRight";
            this.buttonbuyHorsemanRight.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyHorsemanRight.TabIndex = 28;
            this.buttonbuyHorsemanRight.Text = "Купить наездника";
            this.buttonbuyHorsemanRight.UseVisualStyleBackColor = true;
            this.buttonbuyHorsemanRight.Click += new System.EventHandler(this.buttonbuyHorsemanRight_Click);
            // 
            // buttonInfoBardRight
            // 
            this.buttonInfoBardRight.Location = new System.Drawing.Point(24, 147);
            this.buttonInfoBardRight.Name = "buttonInfoBardRight";
            this.buttonInfoBardRight.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoBardRight.TabIndex = 27;
            this.buttonInfoBardRight.Text = "Инфо барда";
            this.buttonInfoBardRight.UseVisualStyleBackColor = true;
            this.buttonInfoBardRight.Click += new System.EventHandler(this.buttonInfoBardRight_Click);
            // 
            // buttonbuyBardRight
            // 
            this.buttonbuyBardRight.Location = new System.Drawing.Point(133, 147);
            this.buttonbuyBardRight.Name = "buttonbuyBardRight";
            this.buttonbuyBardRight.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyBardRight.TabIndex = 26;
            this.buttonbuyBardRight.Text = "Купить барда";
            this.buttonbuyBardRight.UseVisualStyleBackColor = true;
            this.buttonbuyBardRight.Click += new System.EventHandler(this.buttonbuyBardRight_Click);
            // 
            // buttonInfoArcherRight
            // 
            this.buttonInfoArcherRight.Location = new System.Drawing.Point(24, 118);
            this.buttonInfoArcherRight.Name = "buttonInfoArcherRight";
            this.buttonInfoArcherRight.Size = new System.Drawing.Size(103, 23);
            this.buttonInfoArcherRight.TabIndex = 25;
            this.buttonInfoArcherRight.Text = "Инфо лучника";
            this.buttonInfoArcherRight.UseVisualStyleBackColor = true;
            this.buttonInfoArcherRight.Click += new System.EventHandler(this.buttonInfoArcherRight_Click);
            // 
            // buttonbuyArcherRight
            // 
            this.buttonbuyArcherRight.Location = new System.Drawing.Point(133, 118);
            this.buttonbuyArcherRight.Name = "buttonbuyArcherRight";
            this.buttonbuyArcherRight.Size = new System.Drawing.Size(108, 23);
            this.buttonbuyArcherRight.TabIndex = 24;
            this.buttonbuyArcherRight.Text = "Купить лучника";
            this.buttonbuyArcherRight.UseVisualStyleBackColor = true;
            this.buttonbuyArcherRight.Click += new System.EventHandler(this.buttonbuyArcherRight_Click);
            // 
            // listBoxbuyUnitsRight
            // 
            this.listBoxbuyUnitsRight.FormattingEnabled = true;
            this.listBoxbuyUnitsRight.Location = new System.Drawing.Point(21, 384);
            this.listBoxbuyUnitsRight.Name = "listBoxbuyUnitsRight";
            this.listBoxbuyUnitsRight.Size = new System.Drawing.Size(220, 238);
            this.listBoxbuyUnitsRight.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(16, 356);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(225, 25);
            this.label6.TabIndex = 22;
            this.label6.Text = "Купленные существа";
            // 
            // labelMoneyRight
            // 
            this.labelMoneyRight.AutoSize = true;
            this.labelMoneyRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMoneyRight.Location = new System.Drawing.Point(16, 74);
            this.labelMoneyRight.Name = "labelMoneyRight";
            this.labelMoneyRight.Size = new System.Drawing.Size(83, 25);
            this.labelMoneyRight.TabIndex = 21;
            this.labelMoneyRight.Text = "Деньги";
            // 
            // labelNameRight
            // 
            this.labelNameRight.AutoSize = true;
            this.labelNameRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNameRight.Location = new System.Drawing.Point(16, 49);
            this.labelNameRight.Name = "labelNameRight";
            this.labelNameRight.Size = new System.Drawing.Size(53, 25);
            this.labelNameRight.TabIndex = 20;
            this.labelNameRight.Text = "Имя";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(16, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 25);
            this.label9.TabIndex = 19;
            this.label9.Text = "Игрок 2";
            // 
            // panelInfo
            // 
            this.panelInfo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelInfo.Controls.Add(this.labelIJSelected);
            this.panelInfo.Controls.Add(this.buttonSaveAndContinue);
            this.panelInfo.Controls.Add(this.labelFieldSize);
            this.panelInfo.Location = new System.Drawing.Point(274, 589);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(256, 59);
            this.panelInfo.TabIndex = 3;
            // 
            // labelIJSelected
            // 
            this.labelIJSelected.AutoSize = true;
            this.labelIJSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIJSelected.Location = new System.Drawing.Point(69, 17);
            this.labelIJSelected.Name = "labelIJSelected";
            this.labelIJSelected.Size = new System.Drawing.Size(57, 25);
            this.labelIJSelected.TabIndex = 7;
            this.labelIJSelected.Text = "fsize";
            // 
            // buttonSaveAndContinue
            // 
            this.buttonSaveAndContinue.Location = new System.Drawing.Point(132, 19);
            this.buttonSaveAndContinue.Name = "buttonSaveAndContinue";
            this.buttonSaveAndContinue.Size = new System.Drawing.Size(184, 23);
            this.buttonSaveAndContinue.TabIndex = 6;
            this.buttonSaveAndContinue.Text = "Сохранить и продолжить";
            this.buttonSaveAndContinue.UseVisualStyleBackColor = true;
            this.buttonSaveAndContinue.Click += new System.EventHandler(this.buttonSaveAndContinue_Click);
            // 
            // labelFieldSize
            // 
            this.labelFieldSize.AutoSize = true;
            this.labelFieldSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFieldSize.Location = new System.Drawing.Point(6, 17);
            this.labelFieldSize.Name = "labelFieldSize";
            this.labelFieldSize.Size = new System.Drawing.Size(57, 25);
            this.labelFieldSize.TabIndex = 5;
            this.labelFieldSize.Text = "fsize";
            // 
            // FormShop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 661);
            this.Controls.Add(this.panelInfo);
            this.Controls.Add(this.panelPlayerRight);
            this.Controls.Add(this.panelBattleField);
            this.Controls.Add(this.panelPlayerLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormShop";
            this.Text = "FormShop";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormShop_FormClosed);
            this.Load += new System.EventHandler(this.FormShop_Load);
            this.panelPlayerLeft.ResumeLayout(false);
            this.panelPlayerLeft.PerformLayout();
            this.panelBattleField.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBattleField)).EndInit();
            this.panelPlayerRight.ResumeLayout(false);
            this.panelPlayerRight.PerformLayout();
            this.panelInfo.ResumeLayout(false);
            this.panelInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPlayerLeft;
        private System.Windows.Forms.ListBox listBoxbuyUnitsLeft;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelMoneyLeft;
        private System.Windows.Forms.Label labelNameLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelBattleField;
        private System.Windows.Forms.Panel panelPlayerRight;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Button buttonSaveAndContinue;
        private System.Windows.Forms.Label labelFieldSize;
        private System.Windows.Forms.Button buttonInfoWizardLeft;
        private System.Windows.Forms.Button buttonbuyWizardLeft;
        private System.Windows.Forms.Button buttonInfoWarriorLeft;
        private System.Windows.Forms.Button buttonbuyWarriorLeft;
        private System.Windows.Forms.Button buttonInfoHorsemanLeft;
        private System.Windows.Forms.Button buttonbuyHorsemanLeft;
        private System.Windows.Forms.Button buttonInfoBardLeft;
        private System.Windows.Forms.Button buttonbuyBardLeft;
        private System.Windows.Forms.Button buttonInfoArcherLeft;
        private System.Windows.Forms.Button buttonbuyArcherLeft;
        private System.Windows.Forms.PictureBox pictureBoxBattleField;
        private System.Windows.Forms.Button buttonInfoWizardRight;
        private System.Windows.Forms.Button buttonbuyWizardRight;
        private System.Windows.Forms.Button buttonInfoWarriorRight;
        private System.Windows.Forms.Button buttonbuyWarriorRight;
        private System.Windows.Forms.Button buttonInfoHorsemanRight;
        private System.Windows.Forms.Button buttonbuyHorsemanRight;
        private System.Windows.Forms.Button buttonInfoBardRight;
        private System.Windows.Forms.Button buttonbuyBardRight;
        private System.Windows.Forms.Button buttonInfoArcherRight;
        private System.Windows.Forms.Button buttonbuyArcherRight;
        private System.Windows.Forms.ListBox listBoxbuyUnitsRight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelMoneyRight;
        private System.Windows.Forms.Label labelNameRight;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelIJSelected;
        private System.Windows.Forms.Button buttonSellUnitLeft;
        private System.Windows.Forms.Button buttonSellUniRight;
    }
}