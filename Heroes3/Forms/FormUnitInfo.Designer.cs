﻿namespace Heroes3.Forms
{
    partial class FormUnitInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxUnitImage = new System.Windows.Forms.PictureBox();
            this.labelUnitInfo = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUnitImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxUnitImage
            // 
            this.pictureBoxUnitImage.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxUnitImage.Name = "pictureBoxUnitImage";
            this.pictureBoxUnitImage.Size = new System.Drawing.Size(128, 128);
            this.pictureBoxUnitImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUnitImage.TabIndex = 0;
            this.pictureBoxUnitImage.TabStop = false;
            // 
            // labelUnitInfo
            // 
            this.labelUnitInfo.AutoSize = true;
            this.labelUnitInfo.Location = new System.Drawing.Point(148, 13);
            this.labelUnitInfo.Name = "labelUnitInfo";
            this.labelUnitInfo.Size = new System.Drawing.Size(35, 13);
            this.labelUnitInfo.TabIndex = 1;
            this.labelUnitInfo.Text = "label1";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(12, 152);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(366, 23);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // FormUnitInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 185);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelUnitInfo);
            this.Controls.Add(this.pictureBoxUnitImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUnitInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormUnitInfo";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUnitImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxUnitImage;
        private System.Windows.Forms.Label labelUnitInfo;
        private System.Windows.Forms.Button buttonOk;
    }
}